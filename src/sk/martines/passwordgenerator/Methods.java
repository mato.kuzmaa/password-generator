package sk.martines.passwordgenerator;

public class Methods {

    public static String onlyLetters (String number){

        int passwordLength = Integer.parseInt(number);
        String password = "";
        char charOfPassword;

        int max = 122;
        int min = 65;
        int range = max - min + 1;

        for (int i = 0; i < passwordLength; i++) {
            int rand = (int)(Math.random() * range) + min;
            if(rand > 90 && rand < 97) {
                passwordLength++;
                continue;
            }
            charOfPassword = (char)rand;

            password = password+charOfPassword;

        }

        return password;

    }

    public static String onlyNumbers (String cislo) {
        int passwordLength = Integer.parseInt(cislo);
        String password = "";
        char charOfPassword;

        int max = 57;
        int min = 48;
        int range = max - min + 1;

        for (int i = 0; i < passwordLength; i++) {
            int rand = (int)(Math.random() * range) + min;

            charOfPassword = (char) rand;

            password = password+charOfPassword;

        }

        return password;
    }

    public static String onlyCharacters (String cislo) {

        int passwordLength = Integer.parseInt(cislo);
        String password = "";
        char charOfPassword;

        int max = 96;
        int min = 33;
        int range = max - min + 1;

        for (int i = 0; i < passwordLength; i++) {
            int rand = (int)(Math.random() * range) + min;
            if(rand > 47 && rand < 58) {
                passwordLength++;
                continue;
            }else if (rand > 64 && rand < 91) {
                passwordLength++;
                continue;
            }
            charOfPassword = (char) rand;

            password = password+charOfPassword;

        }

        return password;

    }

    public static String charactersLettersNumbers (String cislo) {
        int passwordLength = Integer.parseInt(cislo);
        String password = "";
        char charOfPassword ;

        // define the range, ASCII (a-z = 97-122)
        int max = 122;
        int min = 33;
        int range = max - min + 1;


        for (int i = 0; i < passwordLength; i++) {
            int rand = (int)(Math.random() * range) + min;
            charOfPassword = (char) rand;
            password = password+charOfPassword;

        }
        return password;

    }

    public static String lettersNumbers (String cislo) {
        int passwordLength = Integer.parseInt(cislo);
        String password = "";
        char charOfPassword;

        int max = 122;
        int min = 48;
        int range = max - min + 1;

        for (int i = 0; i < passwordLength; i++) {
            int rand = (int)(Math.random() * range) + min;
            if(rand > 57 && rand < 65) {
                passwordLength++;
                continue;
            }else if (rand > 90 && rand < 97) {
                passwordLength++;
                continue;
            }
            charOfPassword = (char) rand;

            password = password+charOfPassword;

        }
        return password;

    }

    public static String charactersLetters (String cislo) {
        int passwordLength = Integer.parseInt(cislo);
        String password = "";
        char charOfPassword;

        int max = 122;
        int min = 33;
        int range = max - min + 1;

        for (int i = 0; i < passwordLength; i++) {
            int rand = (int)(Math.random() * range) + min;
            if(rand > 47 && rand < 58) {
                passwordLength++;
                continue;
            }
            charOfPassword = (char) rand;

            password = password+charOfPassword;

        }
        return password;

    }

    public static String charactersNumbers (String cislo) {
        int passwordLength = Integer.parseInt(cislo);
        String password = "";
        char charOfPassword;

        int max = 96;
        int min = 33;
        int range = max - min + 1;

        for (int i = 0; i < passwordLength; i++) {
            int rand = (int)(Math.random() * range) + min;
            if(rand > 64 && rand < 91) {
                passwordLength++;
                continue;
            }
            charOfPassword = (char) rand;

            password = password+charOfPassword;

        }
        return password;

    }

}
