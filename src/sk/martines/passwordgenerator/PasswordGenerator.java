package sk.martines.passwordgenerator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.InputMismatchException;

public class PasswordGenerator extends JFrame {
    private JRadioButton characters;
    private JRadioButton letters;
    private JRadioButton numbers;
    private JTextField tfCountCharacters;
    private JLabel lbCountCharacters;
    private JButton generator;
    private JTextField newPassword;


    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Throwable e) {
            e.printStackTrace();
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    PasswordGenerator frame = new PasswordGenerator();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public PasswordGenerator() {

        initComponents();
        createEvents();

    }

    private void createEvents() {
        //Auto-generated method stub
        generator.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                try {
                    String countCharacters = tfCountCharacters.getText();
                    String password = "";

                    if(characters.isSelected() && !numbers.isSelected() && !letters.isSelected()) {
                        password = Methods.onlyLetters(countCharacters);
                    }
                    else if(!characters.isSelected() && numbers.isSelected() && !letters.isSelected()) {
                        password = Methods.onlyNumbers(countCharacters);
                    }
                    else if(!characters.isSelected() && !numbers.isSelected() && letters.isSelected()) {
                        password = Methods.onlyCharacters(countCharacters);
                    }
                    else if(characters.isSelected() && numbers.isSelected() && letters.isSelected()) {
                        password = Methods.charactersLettersNumbers(countCharacters);
                    }
                    else if(characters.isSelected() && numbers.isSelected() && !letters.isSelected()) {
                        password = Methods.lettersNumbers(countCharacters);
                    }
                    else if(characters.isSelected() && !numbers.isSelected() && letters.isSelected()) {
                        password = Methods.charactersLetters(countCharacters);
                    }
                    else if(!characters.isSelected() && numbers.isSelected() && letters.isSelected()) {
                        password = Methods.charactersNumbers(countCharacters);
                    }

                    newPassword.setText(password);

                } catch(InputMismatchException im) {
                    newPassword.setText("Incorrect entry, enter integer");
                } catch(NumberFormatException nfe) {
                    newPassword.setText("Incorrect entry, enter integer");
                }

            }
        });

    }

    private void initComponents() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 375);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        characters = new JRadioButton("Letters");
        characters.setToolTipText("password parameter");
        characters.setFont(new Font("Arial", Font.PLAIN, 18));
        characters.setSelected(true);
        characters.setBounds(24, 32, 131, 38);
        characters.setForeground(Color.BLUE);
        contentPane.add(characters);

        numbers = new JRadioButton("Numbers");
        numbers.setToolTipText("password parameter");
        numbers.setFont(new Font("Arial", Font.PLAIN, 18));
        numbers.setBounds(213, 32, 131, 38);
        numbers.setForeground(Color.BLACK);
        contentPane.add(numbers);

        letters = new JRadioButton("Characters");
        letters.setToolTipText("password parameter");
        letters.setFont(new Font("Arial", Font.PLAIN, 18));
        letters.setBounds(393, 32, 131, 38);
        letters.setForeground(Color.GRAY);
        contentPane.add(letters);

        tfCountCharacters = new JTextField("Count of characters:");
        tfCountCharacters.setFont(new Font("Arial", Font.PLAIN, 18));
        tfCountCharacters.setBounds(24, 119, 200, 26);
        tfCountCharacters.setForeground(Color.GREEN);
        contentPane.add(tfCountCharacters);

        tfCountCharacters = new JTextField();
        tfCountCharacters.setHorizontalAlignment(SwingConstants.CENTER);
        tfCountCharacters.setToolTipText("Enter the whole number");
        tfCountCharacters.setFont(new Font("Arial", Font.PLAIN, 18));
        tfCountCharacters.setBounds(300, 113, 208, 44);
        contentPane.add(tfCountCharacters);
        tfCountCharacters.setColumns(10);

        newPassword = new JTextField();
        newPassword.setHorizontalAlignment(SwingConstants.CENTER);
        newPassword.setEditable(false);
        newPassword.setToolTipText("Your new password");
        newPassword.setFont(new Font("Arial", Font.ITALIC, 20));
        newPassword.setBounds(24, 178, 500, 38);
        contentPane.add(newPassword);
        newPassword.setColumns(10);

        generator = new JButton("Generate password");
        generator.setForeground(Color.RED);


        generator.setToolTipText("Click to generate password");
        generator.setFont(new Font("Arial", Font.PLAIN, 25));
        generator.setBounds(156, 240, 270, 38);
        contentPane.add(generator);

        setTitle("Password generator");

    }
}

